# База Вопросов Интернет-клуба "Что? Где? Когда?"

[Источник]( http://db.chgk.info )

[Лицензия]( https://db.chgk.info/copyright )

### Формат файлов

Кодировка: KOI8-R

[Описание формата]( https://db.chgk.info/format_voprosov )
